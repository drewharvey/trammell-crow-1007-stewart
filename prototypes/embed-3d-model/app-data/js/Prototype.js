function loadEss(filename,svgName,claz,mainWidth,view){
	var viewPicker;
	var width; var height; var aspect;
	var aspect;
	var width=mainWidth;
	var newData=[];var useData=[];var useData2=[];var maxCrv;
	var viewCounter=0;
	var crvCounter=0;
    d3.json(filename, function(error, data) {
		
    	data.forEach(function(kv){  
    		viewCounter++;
	    	newData.push(kv.params);	
    	});
    	viewCount();
    });
    
    function viewCount(){
    	var maxCrvs=[];
    	for(i=0;i<viewCounter;i++){
    		useData=newData[i];
    	}
    	viewRun();
    }

    function viewRun(){
	    useData=newData[view];
	    graphRun();
	};
	
	
	function graphRun(){
	
    	useData.forEach(function(kv,j){  
    		kv.points=[];  
    		aspect=kv.aspectRatio;
			
		    	kv.xyData.forEach(function(d) {		    	
		    		
				    	for (var i = 0; d["x"+i] !== "" && (typeof d["x"+i] !== 'undefined'); i++) {
				        	kv.points.push([mainWidth*d["x"+i], mainWidth/aspect*(d["y"+i])]);	
				    	};
				});
				
				 
		
		});
		
		var	margin = {top: 0, right: 0, bottom: 0, left: 0},	
		width = mainWidth + margin.left - margin.right,				
		height = mainWidth/aspect + margin.top - margin.bottom;	
		// console.log(height);
		d3.select(claz)						
			.attr("width", width)	
			.attr("height", height)
		;
				
		var line = 
		d3.svg.line() 
		;
		
		
			
		var draws=svgName.selectAll("path")
		    .data(useData)
		    ;
		draws
			.exit().transition().duration(3200)
			.attr("transform","scale(0,0)")
			.remove()
			;    
		if(filename=="data/essential.json"){
			draws    
			    .enter()
			    .append("path")
			    .attr("class","paths")
			    .on("mouseover",function(d,z){hoverer2(z);})
			    .on("mouseout",hoverout)
			    ;		
	   }
	   else{
			draws    
			    .enter()
			    .append("path")
			    .attr("class","paths")
			    .on("mouseover",function(d,z){hoverer(z);})
			    .on("mouseout",hoverout)
			    .on("click", zoomBox)
			    ;		
	   }
	   
		draws
			.transition()
			.duration(3200)
			.attr("d", 					function(d){return line(d.points);})
		    .style("stroke-opacity",	function(d){return d.strokeOpacity;})
		    .style("stroke",			function(d){return d3.rgb(d.strokeColor);})
		    .style("stroke-width", 		function(d){return d.strokeWeight;})  
		    .style("stroke-width", 		0)  
		    .style("fill",				function(d){if(d.fillColor==null){return "none";} else{return d3.rgb(d.fillColor);}})
	   		.style("fill-opacity",		function(d){return d.fillOpacity;})		 
			.style("stroke-dasharray",	function(d){if(d.dashed==1){return ("3,2");}})
		    .attr("stroke-linecap","round")
		    .attr("stroke-linejoin","round")
		    
		    ;
	function hoverer(z){
		if(filename!="data/essential.json"){
			d3.select("body").style("cursor", "default");
			draws
				.style("fill",function(d,q){if(q==z){return "black";}else{if(d.fillColor==null){return "none";} else{return d3.rgb(d.fillColor);}}})
				.style("fill-opacity",function(d,q){if(q==z){return .15;}else{{return d.fillOpacity;}}})
				;
			};		
	}
	function hoverer2(z){
		if(z!=0){
			d3.select("body").style("cursor", "pointer");
			draws		
			.on("click", function(){
				window.open("http://lmnts.lmnarchitects.com/");
				// d3.select("body")
				  // .append("a")
				  // .attr("xlink:href", "google.com")
  				  // .attr("target", "_blank")
  				  // ;
  				  console.log('clicked');
			})			
			.on("mouseout",hoverout)
			;
		}
	}
	function hoverout(){
		draws
			.style("fill",				function(d){if(d.fillColor==null){return "none";} else{return d3.rgb(d.fillColor);}})
	   		.style("fill-opacity",		function(d){return d.fillOpacity;})		
			;
		d3.select("body").style("cursor", "default");
		}
	};
};



function loadButtons(mainFile,filename,svgName,mainWidth,view){
	var viewPicker;
	var aspect2;
	var newData=[];var useData=[];
	var crvCounter=0;
    d3.json(filename, function(error, data) {
    	data.forEach(function(kv){  
	    	newData.push(kv.params);	
    	});
    	viewRun();
    });
    
    function viewRun(){
	    useData=newData[view];
	    graphRun();
	};
	
	function graphRun(){   	
    	useData.forEach(function(kv){  
    		kv.points=[];  
    		aspect2=kv.aspectRatio;
		    	kv.xyData.forEach(function(d,z) {		    	
			    	for (var i = 0; d["x"+i] !== "" && (typeof d["x"+i] !== 'undefined'); i++) {
			        	kv.points.push([mainWidth*d["x"+i], mainWidth/aspect2*(d["y"+i])]);
			    	}
				});

		});


	
		var line = 
		d3.svg.line() 
		
		;

		var buttonz=svgName.selectAll("path")
		    .data(useData)
		    .enter()
		    .append("path")
		    .attr("class","buttonz")
		    
		    ;
	    
		buttonz
			.on("click",function(d,z){viewChange(z);})
			.transition()
			.duration(3200)			
		    .attr("d", 					function(d){return line(d.points);})	
			.style("opacity",1)   
		    .attr("stroke-linecap","round")
		    .attr("stroke-linejoin","round")
		    ;
		if(init==0){
			buttonz
				.style("fill", function(d,z){if(z==0){return "black";}})
				;
			init=1;		
		}	
		function viewChange(h){
			loadEss(mainFile,svgEss3,".mainDwg",mainWidth,h);
			buttonz
				.style("fill", function(d,z){if(z==h){return "black";}})
			;
			// console.log(h);
		}
	};
	
	
};


function loadDL(filename,svgName,claz,mainWidth,view){
	var viewPicker;
	var width; var height; var aspect;
	var aspect;
	var newData=[];var useData=[];
	var crvCounter=0;
    d3.json(filename, function(error, data) {
    	data.forEach(function(kv){  
	    	newData.push(kv.params);	
    	});
    	viewRun();
    });
    
    function viewRun(){
	    useData=newData[view];
	    graphRun();
	};
	
	function graphRun(){   	
    	useData.forEach(function(kv){  
    		kv.points=[];  
    		aspect=kv.aspectRatio;
		    	kv.xyData.forEach(function(d) {		    	
			    	for (var i = 0; d["x"+i] !== "" && (typeof d["x"+i] !== 'undefined'); i++) {
			        	kv.points.push([mainWidth*d["x"+i], mainWidth/aspect*(d["y"+i])]);
			    	}
				});

		});
		var	margin = {top: 0, right: 0, bottom: 0, left: 0},	
		width = mainWidth + margin.left - margin.right,				
		height = mainWidth/aspect + margin.top - margin.bottom;	
		
		d3.select(claz)						
			.attr("width", width)	
			.attr("height", height)
		;
				
		var line = 
		d3.svg.line() 
		;

		svgName.selectAll("path")
		    .data(useData)
		    .enter()
		    .append("path")
		    .attr("class","dlpaths")
		    .on("click",dlGo)
			.on("mouseover",dlReady)
		   ;
	    
		svgName.selectAll("path")
			.transition()
			.duration(3200)
			.style("fill","grey")
			.style("fill-opacity",1)
		    .attr("d", 	function(d){return line(d.points);})	
		    .attr("stroke-linecap","round")
		    .attr("stroke-linejoin","round")
		    ;
	};

};
 function dlGo(){
 	window.location="javascript:javascript: (function () { var e = document.createElement('script'); if (window.location.protocol === 'https:') { e.setAttribute('src', 'https://raw.github.com/NYTimes/svg-crowbar/gh-pages/svg-crowbar.js'); } else { e.setAttribute('src', 'http://nytimes.github.com/svg-crowbar/svg-crowbar.js'); } e.setAttribute('class', 'svg-crowbar'); document.body.appendChild(e); })();"
	;
	}	
function dlReady(){
	d3.selectAll(".dlpaths")
		.style("fill","black")
	.on("mouseout",function(){
		d3.selectAll(".dlpaths")
		.style("fill","grey");
	})
	;
}    