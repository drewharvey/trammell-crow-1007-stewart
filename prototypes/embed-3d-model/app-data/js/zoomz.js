
	var transDur=600;
	
	var width = 900,
	    // height = window.innerHeight,
	     height = 740.58,
	   // var width,height;
	    
	    active = d3.select(null);
	var projection = d3.geo.albersUsa()
	    .scale(1000)
	    .translate([width / 2, height / 2]);
	
	var zoom = d3.behavior.zoom()
	    .translate([0, 0])
	    .scale(1)
	    .scaleExtent([1, 8])
	    .on("zoom", zoomed);
	    
	var zoomDraw = d3.behavior.zoom()
	    .translate([0, 0])
	    .scale(1)
	    .scaleExtent([1, 8])
	    .on("zoom", zoomedDraw);
	    
	var path = d3.geo.path()
	    .projection(projection);
	
	var svgFull = d3.select("#selector").append("svg")
	    .attr("width", width)
	    .attr("height", height)
	   
	    .on("click", stopped, true);
	
	
	
	var g = svgFull.append("g");
	
	
function zoomz(){	  
	console.log(svgEss3);  
	svgEss3
		// .call(zoomDraw) // delete this line to disable free zooming
	    .call(zoomDraw.event)
	    ;
	svgFull
	    // .call(zoom) // delete this line to disable free zooming
	    .call(zoom.event)
	    ;
	
	var imgs = 
	g
	// .append("image")
	
	            .append("svgFull:image")
	            .attr("xlink:href", "data/SectionPerspective.jpg")
	            .attr("x", "0")
	            .attr("y", "0")
	            .attr("width", width)
	            .attr("height", height)
        	     .attr("transform", "translate(0,0)")
	            ;
	svgEss3.append("rect")
	    .attr("class", "background")
	    .attr("width", width)
	    .attr("height", height)
	    .style("fill-opacity",0)
	    .on("click", reset);
	 }   
	    
	    
	    
	function zoomBox(d) {
		console.log((d));
		console.log(path.bounds(d));
		// console.log(bounds[0][0]);
		// console.log(d.points[0]);
		console.log(d.points.length);
	 var xs=[]; var ys=[];
	 for(var z=0;z<d.points.length;z++){
	 	xs.push(d.points[z][0]);
	 	ys.push(d.points[z][1]);
	 }
	
	  if (active.node() === this) return reset();
	  active.classed("active", false);
	  active = d3.select(this).classed("active", true);
	// console.log(dx);

	      dx = d3.max(xs)-d3.min(xs),
	      dy = d3.max(ys)-d3.min(ys),
	      x = (d3.min(xs)+d3.max(xs))/2,
	      y = (d3.min(ys)+d3.max(ys))/2,
	     
	      scale = .9 / Math.max(dx / width, dy / height),
	      translate = [width / 2 - scale * x, height / 2 - scale * y];
	      
	  svgEss3.transition()
	      .duration(transDur)
	      .call(zoomDraw.translate(translate).scale(scale).event);		
	      
	  svgFull.transition()
	      .duration(transDur)
	      .call(zoom.translate(translate).scale(scale).event);	 
	}
	
	function reset() {
		console.log("working");
	  active.classed("active", false);
	  active = d3.select(null);
	
	  svgFull.transition()
	      .duration(transDur)
	      .call(zoom.translate([0, 0]).scale(1).event);
	      
	  svgEss3.transition()
	      .duration(transDur)
	      .call(zoomDraw.translate([0, 0]).scale(1).event);		
	      
	}
	
	function zoomed() {
	  // g.style("stroke-width", 1.5 / d3.event.scale + "px");
	  g.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
	}
	function zoomedDraw() {
	  // g.style("stroke-width", 1.5 / d3.event.scale + "px");
	  svgEss3.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
	}
	// If the drag behavior prevents the default click,
	// also stop propagation so we don�t click-to-zoom.
	function stopped() {
	  if (d3.event.defaultPrevented) d3.event.stopPropagation();
	}

