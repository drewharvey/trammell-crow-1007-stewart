$(function() {
  $(document).on('click', '.image-map-area', function() {
    var id = $(this).attr('href').substring(1);
    var $el = $('#' + id);
    if ($el.length < 1) {
      console.log("cant find element with id: " + id);
    }
    ShowModal($el.html());
  });
});

var initImageMap = function($imageMap) {
  console.log('img-map: ' + $imageMap.width());
  console.log('img: ' + $imageMap.children('img').width());
  $imageMap.children('img').load(function() {
    $imageMap.width($(this).width());
  });
}