
var initThirdLevelNav = function() {
  console.log("init third level nav");
  $navs = $('.thirdLevelNav');
  $content = $('.thirdLevelNavContent');
  // set first nav item as active
  $navs.each(function() {
    $(this).children('li').children('a').first().addClass('active');
  });
  // scale all images to screen size
  $content.find('img').each(function() {
    ScaleToFit($(this), $content);
  });
  // show first content element
  $content.each(function() {
    $(this).children('div').hide();
    $(this).children('div').first().show();
  });
}

var updateThirdLevelNav = function(activeNav) {
  var $newActiveNav = $(activeNav);
  $newActiveNav.parent('li').parent('nav').find('a').removeClass('active');
  $newActiveNav.addClass("active");
}

//bind third level nav clicks
$(document).on('click', '.thirdLevelNav a', function() {
  var $wrapper = $(this).parents('.twoColWrapper').children('.thirdLevelNavContent');

  // find content to show based on click
  var contentSelector = $(this).attr("href").substr(1);
  var $contentToShow = $wrapper.find("#" + contentSelector);

  updateThirdLevelNav(this); 

  // hide others
  $contentToShow.siblings().hide();
  // correctly scale image to screen space
  if ($contentToShow.hasClass('image')) {
    ScaleToFit($contentToShow.children('img'), $wrapper);
  }
  // show content
  $contentToShow.fadeIn();
});