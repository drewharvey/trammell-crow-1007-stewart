function ContentGallery() {
  this.galleryItems = [];
  this.currentItemIndex = 0;
}

ContentGallery.prototype.setGalleryItems = function(items) {
  this.items = items;
}

ContentGallery.prototype.getGalleryItemAt = function(index) {
  if (index < 0 || index >= this.galleryItems.length) {
    return null;
  }
  return this.galleryItems[i];
}

// ---------------------------------------------
// GalleryItem Class ---------------------------
// ---------------------------------------------

function GalleryItem() {

}

var GalleryItem = [
  {
      "navTitle": "Tower Rendering",
      "image": "resources/images/design-and-great-room/tower-rendering.jpg"
  }, {
      "navTitle": "Entry Rendering",
      "image": "resources/images/design-and-great-room/entry-rendering.jpg"
  }, {
      "navTitle": "Lobby Rendering",
      "image": "resources/images/design-and-great-room/lobby-rendering.jpg"
  }, {
      "navTitle": "Great Room Rendering",
      "image": "resources/images/design-and-great-room/great-room-rendering.jpg"
  }, {
      "navTitle": "Conference Center",
      "image": "resources/images/design-and-great-room/conference-center.jpg"
  }, {
      "navTitle": "Fitness Center",
      "image": "resources/images/design-and-great-room/fitness-center.jpg"
  }, {
      "navTitle": "Building Views",
      "image": "resources/images/design-and-great-room/building-views.jpg"
  }
]
