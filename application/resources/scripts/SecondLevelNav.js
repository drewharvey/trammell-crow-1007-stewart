var initSecondLevelNav = function() {
  console.log("init second level nav");
  $navs = $('#subNav');
  $content = $('#subContentWrapper');
  // set first nav item as active
  $navs.each(function() {
    $(this).children('li').children('a').first().addClass('active');
  });
  // show first content element
  $content.each(function() {
    
    $(this).children('div').hide();
    $(this).children('div').first().show();
  });
}

var updateSecondLevelNav = function(activeNav) {
  $("#subNav a").removeClass("active");
  $(activeNav).addClass("active");
}

// bind subnav clicks
$(document).on("click", "#subNav a", function () {
  var $wrapper = $("#subContentWrapper");
  var $images = $("#subContentImages");

  // find content to show based on click
  var contentSelector = $(this).attr("href").substr(1);
  var $contentToShow = $wrapper.find("#" + contentSelector);

  updateSecondLevelNav(this); 

  // hide others
  $contentToShow.siblings().hide();

  // correctly scale image to screen space
  if ($contentToShow.hasClass('image')) {
    console.log("wrapper dim: " + $wrapper.width() + ", " + $wrapper.height());
    ScaleToFit($contentToShow.children('img'), $wrapper);
  }

  // show content
  $contentToShow.fadeIn();
  
  // update sidebar images
  if ($images.length > 0) {
    $images.find("div").hide();
    $images.find("#" + contentSelector).show();
    $images.find("#" + contentSelector).children().show();
  }
});