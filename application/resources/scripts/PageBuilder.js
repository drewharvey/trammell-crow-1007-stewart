function PageBuilder() { 
  // constructor
}

PageBuilder.PageType = {
  ImageGallery: "Desc: View a single image at a time, sub nav on the left side.",
  MiniImageGallery: "Desc: Single image with right hand small nav and next function"
}

PageBuilder.prototype.getHtml = function(pageItems, pageType) {
  var htmlGenerator = this.getGenerator(pageType);
  if (typeof htmlGenerator == undefined) {
    return "error";
  }
  return htmlGenerator(pageItems);
}

PageBuilder.prototype.createPageItem = function() {
  var galleryItem = {
    title: "",
    navTitle: "",
    image: "",
    imageCaption: "",
    pageText: ""
  }
  return galleryItem;
}

PageBuilder.prototype.getGenerator = function(pageType) {
  switch (pageType) {
    case PageBuilder.PageType.ImageGallery:
      return this.generateImageGallery;
    case PageBuilder.PageType.MiniImageGallery:
      return this.generateMiniImageGallery;
    default:
      return null;
  }
}

PageBuilder.prototype.generateImageGallery = function(content) {
  // use string concat instead of DOM creation due to better performance:
  // http://stackoverflow.com/questions/2690352/which-is-better-string-html-generation-or-jquery-dom-element-creation
  var html = "";
  // page wrapper
  html += '<div class="bodyWrapper">';
  html += '<div class="twoColWrapper">';

  // left nav wrapper
  html += '<div class="colSmall">';
  html += '<div class="imgGallery text">';
  // nav items
  for (var i = 0; i < content.length; i++) {
    html += '<div class="image" data-gallery-index="' + i + '">';
    if (content[i].image.length !== undefined) {
      html += '<img src="' + content[i].image + '" />';
    }
    if (content[i].navTitle !== undefined) {
      html += '<span>' + content[i].navTitle + '</span>';
    }
    html += '</div>';
  }
  // close nav wrapper
  html += '</div>';
  html += '</div>';

  // selected image (main content)
  html += '<div class="colLarge">';
  html += '<div class="image">';
  html += '<img class="galleryMain" data-lightbox="true" src="" />';
  html += '</div>';
  html += '</div>';

  // close page wrapper
  html += '</div>';
  html += '</div>';
  return html;
}

PageBuilder.prototype.generateMiniImageGallery = function(content) {
  var html = "";
  html += "<ul>";
  for (var i = 0; i < content.length; i++) {
      html += '<li><a href="#" class="navItem" data-nav-index="' + i + '">' + content[i].navTitle + '</a></li>';
  }
  html += '<li><a href="#" id="next">Next</a></li>'
  html += "</ul>";
  return html;
}